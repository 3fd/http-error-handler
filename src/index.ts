import { Response, Request } from 'express';
import * as log from 'fancy-log';

export interface ErrorResponse {
  code: number;
  error: string;
  reason: string;
  message: string;
  details?: any[];
}

export const badRequest: ErrorResponse = {
  code: 400,
  error: 'Bad Request',
  reason: 'badRequest',
  message: 'The request is invalid',
  details: [],
};

export const invalidParameter: ErrorResponse = {
  code: 400,
  error: 'Bad Request',
  reason: 'invalidParameter',
  message: 'The value for one of the URL parameters was invalid',
  details: [],
};

export const parseError: ErrorResponse = {
  code: 400,
  error: 'Bad Request',
  reason: 'parseError',
  message: 'Could not parse the body of the request according to the provided Content-Type',
  details: [],
};

export const requiredParameter: ErrorResponse = {
  code: 400,
  error: 'Bad Request',
  reason: 'requiredParameter',
  message: 'A required URL parameter or required request body JSON property is missing',
  details: [],
};

export const authError: ErrorResponse = {
  code: 401,
  error: 'Unauthorized',
  reason: 'authError',
  message: 'The authorization provided in the request is invalid',
  details: [],
};

export const requiredAuthorisation: ErrorResponse = {
  code: 401,
  error: 'Unauthorized',
  reason: 'requiredAuthorisation',
  message: 'The request requires authorization, but none was provided',
  details: [],
};

export const forbidden: ErrorResponse = {
  code: 403,
  error: 'Forbidden',
  reason: 'forbidden',
  message: 'The user does not have access to perform the request',
  details: [],
};

export const rateLimitExceeded: ErrorResponse = {
  code: 403,
  error: 'Forbidden',
  reason: 'rateLimitExceeded',
  message: 'The rate limit was exceeded. Retry using exponential backoff',
  details: [],
};

export const sslRequired: ErrorResponse = {
  code: 403,
  error: 'Forbidden',
  reason: 'sslRequired',
  message: 'Requests to this API require SSL',
  details: [],
};

export const notFound: ErrorResponse = {
  code: 404,
  error: 'Not Found',
  reason: 'notFound',
  message: 'The resource could not be found',
};

export const internalError: ErrorResponse = {
  code: 500,
  error: 'Internal Server Error',
  reason: 'internalError',
  message: 'The request failed due to an internal error. Try again using truncated exponential backoff.',
};

export const backendError: ErrorResponse = {
  code: 503,
  error: 'Service Unavailable',
  reason: 'backendError',
  message: 'The service needed to fullfill the request is unavailable. Try again using truncated exponential backoff',
};

export const errorHandler = (err: any, req: Request, res: Response, next?: any) => {
  if (next && res.headersSent) {
    return next(err);
  }
  if (err.error && err.error.code) {
    err = err.error;
  }
  else if (err.array) {
    err = { ...badRequest, details: err.useFirstErrorOnly().array() };
  }
  else if (!err.code) {
    log.error(err);
    err = internalError;
  }
  return res.status(err.code).json(err);
};

export const errorHandlerCallback = (req: Request, res: Response, next?: any): (err: any) => Response => {
  return ((err: any) => {
    return errorHandler(err, req, res, next);
  });
};

export const notFoundHandler = (req: Request, res: Response): Response =>
  res.status(notFound.code).json(notFound);
